package com.company;
import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Platform;

public class Main {

    public interface CLibrary extends Library {
        CLibrary INSTANCE = (CLibrary)
                Native.loadLibrary(("dll_test"),
                        CLibrary.class);


        void FooFuncFirst();
    }

    public interface NativeMath extends Library {
        void FooFuncFirst();
    }

    public static void main(String[] args) {

        System.out.println("Simple code");

        CLibrary.INSTANCE.FooFuncFirst();
    }
}